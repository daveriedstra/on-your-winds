{{--
  Template Name: Media Entry Page
  This is the page that uses the bins. For now, there's just the one.
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @php the_content() @endphp
  @endwhile
@endsection

@section('media-page-bar')
  <div class="media-page-frame media-page-frame--hidden"></div>
  <div class="media-page-bottom-bar">
    <button class="dom-logger-toggle">
      Toggle Log
    </button>
    <button class="media-page-play-toggle" disabled>
      Play
    </button>
  </div>
@endsection
