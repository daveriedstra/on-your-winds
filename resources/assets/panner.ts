import { Howl } from 'howler';
import { HowlReference } from 'pool-time';

import { Subject, Observable, pipe } from 'rxjs'
import { filter, take, map } from 'rxjs/operators'

export interface IPanToArgs {
  howlRef: HowlReference;
  start: number; // -1 to 1
  end: number; // -1 to 1
  duration: number; // ms
}

export interface IPanningSound {
  howlRef: HowlReference;
  start: number; // -1 to 1
  end: number; // -1 to 1
  delta: number;
  current: number; // -1 to 1
  increasing: boolean;
  duration: number; // ms
  id: number;
}

/**
 * Coordinates are unitless for 3d audio
 * points of reference for defaults are
 *
 * 10 000 - distance at which sound is inaudible
 * 1 - distance at which sound has full amplitude
 *
 * 3-5 range is good for testing
 *
 * Axes:
 * X - positive toward right
 * Y - positive upward
 * Z - positive behind
 *
 *    y
 *    ↑
 *    |
 *    |
 *    |----→ x
 *   /
 *  ↙
 * z
 *
 */
export interface IMovingVector {
  start: number
  end: number
  delta: number
  current: number
  increasing: boolean
}

interface ICurvable {
  ctlX?: number;
  ctlY?: number;
  ctlZ?: number;
}

export interface IMoveToArgs extends ICurvable {
  howlRef: HowlReference;

  startX: number;
  endX: number;

  startY: number;
  endY: number;

  startZ: number;
  endZ: number;

  duration: number; // ms
}

export interface IMovingSound extends ICurvable {
  howlRef: HowlReference;
  x: IMovingVector
  y: IMovingVector
  z: IMovingVector
  duration: number; // ms
  currentTime: number; // ms
  id: number;
}

/**
 * Use this class to manage all your panning needs.
 */
export class Panner {

  static PANNING_SOUNDS: IPanningSound[] = []
  static MOVING_SOUNDS: IMovingSound[] = []

  static LAST_FRAME_TIME: number = 0
  static FRAME_HANDLER_RUNNING = false

  static panningComplete$ = new Subject<number>()
  static movementComplete$ = new Subject<number>()

  /**
   * Moves a Howl from start to end over duration
   */
  static moveTo({
    howlRef,
    startX, endX,
    startY, endY,
    startZ, endZ,
    duration,
    ctlX, ctlY, ctlZ
  } : IMoveToArgs) : Observable<void> {
    const id = performance.now()
    Panner.MOVING_SOUNDS.push({
      howlRef: howlRef,
      x: {
        start: startX,
        end: endX,
        delta: endX - startX,
        current: startX,
        increasing: startX < endX
      },
      y: {
        start: startY,
        end: endY,
        delta: endY - startY,
        current: startY,
        increasing: startY < endY
      },
      z: {
        start: startZ,
        end: endZ,
        delta: endZ - startZ,
        current: startZ,
        increasing: startZ < endZ
      },
      duration: duration,
      currentTime: 0,
      ctlX: ctlX,
      ctlY: ctlY,
      ctlZ: ctlZ,
      id: id
    })

    Panner.startFrameHandler()

    return Panner.movementComplete$.pipe(
      filter(x => x === id),
      take(1),
      map(x => null)
    )
  }


  /**
   * Pans a Howl from start to end over duration
   */
  static panTo({ howlRef, start, end, duration } : IPanToArgs) {
    // ensure in bounds
    start = Math.max(start, -1)
    start = Math.min(start, 1)
    end = Math.max(end, -1)
    end = Math.min(end, 1)

    if (start === end)
      return

    const id = performance.now()
    Panner.PANNING_SOUNDS.push({
      howlRef: howlRef,
      start: start,
      end: end,
      delta: end - start,
      current: start,
      increasing: start < end,
      duration: duration,
      id: id,
    })

    Panner.startFrameHandler()

    return Panner.panningComplete$.pipe(
      filter(x => x === id),
      take(1),
      map(x => null)
    )
  }

  /**
   * Runs every frame
   */
  static frameHandler(now) {
    const frameDuration = now - Panner.LAST_FRAME_TIME
    Panner.LAST_FRAME_TIME = now

    Panner.updatePanning(frameDuration)
    Panner.updateMovement(frameDuration)

    if (Panner.workToDo()) {
      requestAnimationFrame(Panner.frameHandler)
    } else {
      Panner.FRAME_HANDLER_RUNNING = false
    }
  }

  static startFrameHandler() {
    if (!Panner.FRAME_HANDLER_RUNNING) {
      Panner.FRAME_HANDLER_RUNNING = true
      Panner.LAST_FRAME_TIME = performance.now()
      requestAnimationFrame(Panner.frameHandler)
    }
  }

  /**
   * Updates panning
   */
  static updatePanning(frameDuration: number) {
    Panner.PANNING_SOUNDS.forEach((params, i) => {
      const stepProportion = frameDuration / params.duration
      const newPos = params.current + (params.delta * stepProportion)
      params.howlRef.howl.stereo(newPos, params.howlRef.id)
      params.current = newPos

      if ((params.increasing && newPos >= params.end) || (!params.increasing && newPos <= params.end)) {
        Panner.PANNING_SOUNDS.splice(i, 1)
        Panner.panningComplete$.next(params.id)
      }
    })
  }

  /**
   * Updates position of a sound.
   *
   * This is not 100% precise but we don't need it to be
   */
  static updateMovement(frameDuration: number) {
    Panner.MOVING_SOUNDS.forEach((params, i) => {
      const stepProportion = frameDuration / params.duration
      params.currentTime += frameDuration

      let newX, newY, newZ
      const t = params.currentTime / params.duration

      // arbitrary bezier curving individual vectors :boom:
      if (params.ctlX === undefined)
        newX = params.x.current + (params.x.delta * stepProportion)
      else
        newX = Panner.quadraticBezier(t, params.x.start, params.ctlX, params.x.end)

      if (params.ctlY === undefined)
        newY = params.y.current + (params.y.delta * stepProportion)
      else
        newY = Panner.quadraticBezier(t, params.y.start, params.ctlY, params.y.end)

      if (params.ctlZ === undefined)
        newZ = params.z.current + (params.z.delta * stepProportion)
      else
        newZ = Panner.quadraticBezier(t, params.z.start, params.ctlZ, params.z.end)

      params.howlRef.howl.pos(newX, newY, newZ, params.howlRef.id)

      params.x.current = newX
      params.y.current = newY
      params.z.current = newZ

      if (params.currentTime >= params.duration) {
        Panner.MOVING_SOUNDS.splice(i, 1)
        Panner.movementComplete$.next(params.id)
      }
    })
  }

  /**
   * Is there work to do in the frame handler?
   */
  static workToDo() {
    return Panner.PANNING_SOUNDS.length > 0 || Panner.MOVING_SOUNDS.length > 0
  }

  /**
   * Use this to plot a cartesian value of a point along a 3-point Bezier curve.
   *
   * t = percentage along the curve, 0-1
   *
   * (nts - if we end up needing cubic or greater, npm i bezier)
   */
  static quadraticBezier(t, x1, x2, x3): number {
    return (1 - t) ** 2 * x1 + 2 * (1 - t) * t * x2 + t ** 2 * x3
  }
}
