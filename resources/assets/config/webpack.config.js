const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const webpack = require('webpack');

// const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
    // mode: isProduction ? 'production' : 'development',
    entry: {
      global: './resources/assets/global.ts',
      'media-page': './resources/assets/media-page.ts'
    },
    devtool: 'inline-source-map',
    plugins: [
      new MiniCssExtractPlugin(),
      // new webpack.EnvironmentPlugin({
      //   NODE_ENV: 'development'
      // })
    ],
    module: {
        rules: [{
            test: /\.ts$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }, {
            test: /\.m?js$/,
            type: 'javascript/auto',
            resolve: {
              fullySpecified: false
            }
        }, {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader']
        }]
    },
    resolve: {
        extensions: [ '.ts', '.js' ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(process.cwd(), 'dist')
        // path: path.resolve(__dirname, 'dist')
    },
    devServer: {
        host: '0.0.0.0',
        useLocalIp: true
    }
};
