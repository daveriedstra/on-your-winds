import { Subject, Observable, from } from 'rxjs';

export class PausableTimeout {
  complete$: Observable<void>;

  private _complete$ = new Subject<void>();
  private callback = () => { this._complete$.next() };
  private remainingDuration;
  private lastStarted;
  private timeoutHandle;

  constructor(durationMs: number) {
    this.complete$ = from(this._complete$);
    this.remainingDuration = durationMs;
  }

  /**
   * Call to start or resume the timeout
   */
  start() {
    this.timeoutHandle = window.setTimeout(this.callback, this.remainingDuration);
    this.lastStarted = window.performance.now();
  }

  /**
   * Call to pause the timeout
   */
  pause() {
    const now = window.performance.now();
    const elapsed = now - this.lastStarted;
    this.remainingDuration = this.remainingDuration - elapsed;
    window.clearTimeout(this.timeoutHandle);

    console.dir("paused...")
  }
}
