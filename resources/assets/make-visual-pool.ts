import { PoolDef, BinAsset, VisualBinDef } from './rest-utils';
import {
  getPoolVisualBins,
  makePoolParams,
  makeElementsForVisualBin,
  minMaxToRange,
  dispMs,
  randomElementFromArray,
} from './utils';
import { DomLogger } from './dom-logger';
import { makeRandom3dPos } from './make-audio-pool';
import { PausableTimeout } from './pausable-timeout';
import { Animator } from './animator';

import { Pool } from 'pool-time';
import { Subscription } from 'rxjs';
import { take, takeUntil, first } from 'rxjs/operators';

let logger = new DomLogger();
let mediaFrameEl;
let animator = new Animator();

export async function makeVisualPool(poolDef: PoolDef): Promise<Pool> {
  if (mediaFrameEl === undefined) {
    mediaFrameEl = document.getElementsByClassName("media-page-frame")[0];
  }

  let binDefs = await getPoolVisualBins(poolDef.visual_bins || []);
  binDefs.forEach(b => {
    b.images = [].concat(b.images)
    b.sub_bins = [].concat(b.sub_bins)
  })
  let params = makePoolParams(poolDef);

  let binElPairs = await Promise.all(binDefs.map(async bd => ({
      bin: bd,
      elements: await makeElementsForVisualBin(bd)
    })
  ));

  let pool = new Pool({
    duration: params.duration,
    dropCount: params.dropCount,
    loop: params.loop,
  })

  pool.drop$.subscribe(d => {
    // for each drop, choose a random element from a random bin (usually only one)
    // and display according to that bin's definition

    // 1. choose elements
    let choicePair = randomElementFromArray(binElPairs)
    let choiceElement = randomElementFromArray(choicePair.elements).cloneNode(true) as HTMLDivElement;

    logger.log('visual drop')
    logger.log(choicePair.bin)
    logger.log(choiceElement)

    // determine duration
    // default to pool duration
    let duration = pool.duration / 1000;
    let dur = {
      min: +(poolDef.drop_duration_min || '0').trim(),
      max: +(poolDef.drop_duration_max || '0').trim()
    };
    if (dur.max != 0) {
      let durationRange = minMaxToRange(dur.min, dur.max);
      duration = Math.random() * durationRange.range + durationRange.offset;
    }

    // 2. apply css
    applyVisualCss(choiceElement, choicePair.bin, duration - 0.1);

    // 3. place onscreen
    mediaFrameEl.appendChild(choiceElement);

    // ...and then begin fade in
    window.setTimeout(() => choiceElement.style.opacity = choicePair.bin.opacity, 10);

    // Fade out and remove at the appointed time
    const removeFn = fadeOutAndRemove.bind(undefined, choiceElement, +choicePair.bin.fade_out_time);
    let defaultSub: Subscription;
    if (poolDef.chain_drops) { // if shown until next drop
      defaultSub = pool.drop$.pipe(take(1))
        .subscribe(removeFn);
    } else { // if a fixed duration
      // wait for duration then remove
      let timeout = new PausableTimeout(duration * 1000);
      defaultSub = timeout.complete$.subscribe(removeFn);

      pool.pause$.pipe(
        takeUntil(timeout.complete$)
      ).subscribe(() => timeout.pause());

      pool.resume$.pipe(
        takeUntil(timeout.complete$)
      ).subscribe(() => timeout.start());

      timeout.start();
    }

    // remove at end of pool if necessary
    if (poolDef.contain) {
      defaultSub.unsubscribe();
      pool.stop$.pipe(first())
        .subscribe(removeFn);
    }

    // pause and resume animations
    if (choicePair.bin.has_movement) {
      let pauseFn = function(el) { el.style.animationPlayState = 'paused' }
      let resumeFn = function(el) { el.style.animationPlayState = 'running' }

      pool.pause$ // TODO: only take until element is removed
        .subscribe(pauseFn.bind(undefined, choiceElement));
      pool.resume$ // TODO: only take until element is removed
        .subscribe(resumeFn.bind(undefined, choiceElement));
    }
  });

  pool.start$.subscribe(() => logger.log(`visual pool start: ${pool.dropCount} drops, ${dispMs(pool.duration)} long, loop: ${pool.loop}`))

  return pool
}

// Applies the appropriate CSS to an element according to its bin.
// Modifies the element in place.
function applyVisualCss(el: HTMLElement, bd: VisualBinDef, duration: number) {
  // position
  let startPos = makeRandom3dPos({
    x: +bd.start_x, y: +bd.start_y, z: 0
  }, +bd.start_radius);
  let top = startPos.y;
  let left = startPos.x;
  el.style.top = `${top}vh`;
  el.style.left = `${left}vw`;

  // movement
  // movement duration needs to include fade in and fade out times
  duration += (+bd.fade_in_time || 0) + (+bd.fade_out_time || 0)
  if (bd.has_movement) {
    let endPos = makeRandom3dPos({
      x: +bd.end_x, y: +bd.end_y, z: 0
    }, +bd.start_radius);
    // data comes in expressed as a location but we need to animate translate(),
    // ie, relative, so we need to determine relative distance
    let deltaX = endPos.x - startPos.x;
    let deltaY = endPos.y - startPos.y;
    animator.moveElementTo({
      el: el,
      x: `${deltaX}vw`,
      y: `${deltaY}vh`,
      duration: `${duration}s`,
    });
  }

  let width = +bd.width;
  let height = +bd.height;
  if (bd.variable_size) {
    let widthRange = minMaxToRange(+bd.width_min, +bd.width_max);
    width = Math.random() * widthRange.range + widthRange.offset;
    let heightRange = minMaxToRange(+bd.height_min, +bd.height_max);
    height = Math.random() * heightRange.range + heightRange.offset;
  }
  el.style.height = `${height}vh`;
  el.style.width = `${width}vw`;
  el.style['font-size'] = `${height}vh`;

  // ensure image dimensions are proportional, **increasing** size if needed
  if (el.firstElementChild.tagName.toLowerCase() === 'img') {
    let img = el.firstElementChild as HTMLImageElement;
    let widthToHeight = img.width / img.height;
    let minWidth = widthToHeight * height;
    el.style.minWidth = `${minWidth}vh`; // relative to height
    let minHeight = width / widthToHeight;
    el.style.minHeight = `${minHeight}vw`; // relative to width

    delete img.width;
    delete img.height;
  }

  el.style.zIndex = bd.z_index || "1";

  el.style.opacity = "0";
  el.style.transition = `opacity ${bd.fade_in_time}s`;
}

// Fades an element out and then removes it from the DOM
function fadeOutAndRemove(el: HTMLElement, fadeOutTime: number) {
  if (el.isConnected) { // only try to remove if element is connected to DOM
    fadeOutTime -= 0.05;
    el.style.transition = `opacity ${fadeOutTime}s`;

    window.setTimeout(() => el.style.opacity = "0", 5);

    if (fadeOutTime <= 1) {
      // remove by timeout to avoid firefox missing transitionend bug
      window.setTimeout(() => el.remove(), fadeOutTime * 1000);
    } else {
      // remove with transitionend
      let removeFromDom = e => {
        if (e.propertyName == 'opacity') {
          el.remove();
          el.removeEventListener('transitionend', removeFromDom);
          el.removeEventListener('transitioncancel', removeFromDom);
          let styleElId = el.dataset['keyframesStyle'];
          let styleEl = document.getElementById(styleElId)
          if (!!styleEl && styleEl.isConnected)
            styleEl.remove();
        }
      };
      el.addEventListener('transitionend', removeFromDom);
      el.addEventListener('transitioncancel', removeFromDom);
    }
  }
}
