import { getAudioBin, getVisualBin, getMedia, PoolDef, AudioBinDef, VisualBinDef, WpPostObj, WpMedia, BinAsset } from './rest-utils';

/**
 * Makes a generic set of parameters for a PoolDef useful for starting a pool
 */
export function makePoolParams(def: PoolDef) {
  let durationRange = minMaxToRange(Number.parseFloat(def.duration_min), Number.parseFloat(def.duration_max))
  let dropRange = minMaxToRange(Number.parseFloat(def.drop_count_min), Number.parseFloat(def.drop_count_max))
  return {
    duration: () => {
      return (Math.random() * durationRange.range + durationRange.offset) * 1000
    },
    dropCount: () => {
      return Math.round(Math.random() * dropRange.range + dropRange.offset)
    },
    loop: def.loop
  }
}

/**
 * Gets audio bins for a pool
 */
export async function getPoolAudioBins(bins: WpPostObj[]): Promise<AudioBinDef[]> {
  let binIds = bins.map(b => b.ID)
  return Promise.all(binIds.map(async id => (await getAudioBin(id)).acf))
}

/**
 * Gets visual bins for a pool
 */
export async function getPoolVisualBins(bins: WpPostObj[]): Promise<VisualBinDef[]> {
  let binIds = bins.map(b => b.ID)
  return Promise.all(binIds.map(async id => (await getVisualBin(id)).acf))
}

/**
 * Given an AudioBinDef, make an array of BinAssets representing its sources
 */
export async function makeAssetsForAudioBin(bin: AudioBinDef): Promise<BinAsset[]> {
  let srcReqs: Promise<WpMedia>[];
  // convert bins into an array of requests for their sources
  srcReqs = bin.audio.map(async s => {
    return await getMedia(s.ID)
  });

  // execute requests
  let requests = await Promise.all(srcReqs);

  // create an Asset for each src
  return requests.map(s => ({
    name: s.title.rendered,
    binDef: bin,
    srcs: [s.source_url],
    // seek: duration => Math.random() * duration,
    rate: 1,
  }))
}

export async function makeElementsForVisualBin(bin: VisualBinDef): Promise<HTMLElement[]> {
  // make requests for the WpMedia (post obj) for each img
  let srcReqs = bin.images.filter(x => !!x)
    .map(async s => {
      return await getMedia(s.ID)
    });

  // execute requests
  let wpMediaObjs = await Promise.all(srcReqs);

  // turn them into <img> elements
  let imgs = wpMediaObjs.map(makeImgElement);

  // oh and don't forget the text ones too
  let txts = makeTxtElements(bin);

  // return all the elements
  return [].concat(imgs, txts);
}

function makeImgElement(media: WpMedia): HTMLDivElement {
  let div = document.createElement("div");
  div.className = "media-page-visual-element";

  let el = document.createElement("img");
  el.src = media.guid.rendered; // I think... TODO: sizes
  el.alt = media.alt_text;
  el.className = "media-page-visual-element__img";
  el.width = +media.media_details.width;
  el.height = +media.media_details.height;

  div.appendChild(el);

  return div;
}

function makeTxtElements(binDef: VisualBinDef): HTMLDivElement[] {
  return binDef.text.split("\n")
    .filter(t => t.length > 0)
    .map(txt => {
      let div = document.createElement("div");
      div.className = "media-page-visual-element";

      let el = document.createElement("span")
      el.appendChild(document.createTextNode(txt))
      el.className = "media-page-visual-element__txt";

      div.appendChild(el)
      return div
    })
}

/**
 * Turns a min & max value pair to a range and an offset
 */
export function minMaxToRange(min: number, max: number): { range: number, offset: number } {
  let range = max - min;
  let offset = min;
  return {
    range, offset
  }
}

/**
 * Turns a ms number into a string displaying it as 00 min 00.000 sec
 */
export function dispMs(ms: number): string {
  let sec = ms / 1000;
  return `${(sec / 60).toFixed()} min ${(sec % 60).toFixed(3)} sec`
}

/**
 * Selects and returns a random element from the provided array
 */
export function randomElementFromArray<T>(arr: T[]): T {
  return arr[Math.floor(Math.random() * arr.length)];
};
