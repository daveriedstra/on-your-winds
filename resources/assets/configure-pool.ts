import { getPool, PoolDef } from './rest-utils';
import { makeAudioPool } from './make-audio-pool';
import { makeVisualPool } from './make-visual-pool';
import { Panner } from './panner';
import { makePoolParams, minMaxToRange, dispMs } from './utils';
import { DomLogger } from './dom-logger';

import { Pool, AudioPool, HowlReference } from 'pool-time';
import { take } from 'rxjs/operators';

let logger = new DomLogger();

/**
 * Configures the pools for a PoolDef. There are up to three pools:
 * - an AudioPool for all of the bins
 * - a Pool for all the sub-pools
 * - a Pool for the texts
 */
export async function configurePool(def: PoolDef): Promise<Pool[]> {
  let outPools = [];

  // SubPool Pool
  if (!!def.pools) {
    outPools.push(await makeSubPoolPool(def));
  }

  // Audio Pool
  if (!!def.audio_bins) {
    outPools.push(await makeAudioPool(def));
  }

  // Text pool
  if (!!def.visual_bins) {
    outPools.push(await makeVisualPool(def));
  }

  return outPools;
}

/**
 * Makes a Pool of a parent PoolDef's sub-pools. Recurses by calling configurePool for each sub-pool.
 * NB: The returned Pool is relative to the parent def, and each drop fired starts a sub-pool.
 */
async function makeSubPoolPool(parentDef: PoolDef): Promise<Pool> {
  const childDefReqs = parentDef.pools.map(async p => await getPool(p.ID))
  const childDefs = (await Promise.all(childDefReqs))
  const params = makePoolParams(parentDef)

  let pool = new Pool(params);
  pool.drop$.subscribe(async () => {
    // get a sub pool definition at random
    // alternatively, we could use the index and move through in order
    // heck, we could even make that an option in the UI (random vs ordered)
    const subDef = childDefs[Math.floor(Math.random() * childDefs.length)]
    logger.log("starting sub-Pool")
    logger.logPoolDefPage(subDef)
    const subPool = await configurePool(subDef.acf)

    subPool.forEach(p => p.start())

    // ensure pauses and resumes are propagated to sub-pools
    pool.pause$.subscribe(() => {
      subPool.forEach(p => p.pause())
    })

    pool.resume$.subscribe(() => {
      subPool.forEach(p => p.resume())
    })
  })

  return pool
}
