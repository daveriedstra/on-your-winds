import './media-page.css';

import { getPage, getPool, getMedia, PoolDef, AudioBinDef, WpPostObj, WpPage, WpMedia } from './rest-utils';
import { configurePool } from './configure-pool';
import { DomLogger } from './dom-logger';
import { Pool } from 'pool-time';

export {};

let logger: DomLogger;

/**
 * First, get page data, then get pools for the page.
 */
export async function getEntryPoolDefs(): Promise<WpPage<PoolDef>[]> {
  let entryPools = (await getPage<{pools: WpPostObj[]}>(window['OYW'].page_id)).acf.pools
  return await Promise.all(entryPools.map(async p => getPool(p.ID)))
}

/**
 * get entry pool json from server
 * make a pool representing that json
 * start that pool (or make button to start it)
 */
async function main() {
  logger = new DomLogger();
  let entryPoolDefs = await getEntryPoolDefs();

  logger.log("entry pools:")
  for (let p of entryPoolDefs) {
    logger.logPoolDefPage(p);
  }

  const entryPoolsReqs = entryPoolDefs.map(page => configurePool(page.acf))
  const entryPools = (await Promise.all(entryPoolsReqs))
    .reduce((a, b) => a.concat(b), []);

  enablePlayToggle(entryPools);

  const destroyContent = e => {
    e.target.removeEventListener("transitionend", destroyContent)
    e.target.remove()
  };
  document.getElementsByClassName("content")[0].addEventListener("transitionend", destroyContent)
}

function enablePlayToggle(pools: Pool[]) {
  const contentBtn = document.getElementById("media-page-start-button") as HTMLButtonElement
  const barBtn = document.getElementsByClassName("media-page-play-toggle")[0] as HTMLButtonElement
  contentBtn.disabled = false;
  barBtn.disabled = false;
  barBtn.innerText = "Play";
  let started = false;
  let playing = false;

  const handler = () => {
    if (playing) {
      pools.forEach(p => p.pause())
      barBtn.innerText = "Play";
      playing = false;
    } else {
      if (started) {
        pools.forEach(p => p.resume())
      } else {
        // start page
        pools.forEach(p => p.start())
        started = true;

        // hide content
        contentBtn.removeEventListener("click", handler)
        document.getElementsByClassName("content")[0].classList.add("content--hidden")

        // bring frame to fore
        document.getElementsByClassName("media-page-frame")[0]
          .classList.remove("media-page-frame--hidden");
      }
      barBtn.innerText = "Pause";
      playing = true;
    }
  }

  barBtn.addEventListener("click", handler)
  contentBtn.addEventListener("click", handler)
}


/**
 * Start everything
 */
main()
