import { PoolDef, AudioBinDef, BinAsset } from './rest-utils';
import { Panner } from './panner';
import { DomLogger } from './dom-logger';
import { getPoolAudioBins, makePoolParams, makeAssetsForAudioBin, minMaxToRange, dispMs } from './utils';
import { Pool, AudioPool, HowlReference } from 'pool-time';
import { take } from 'rxjs/operators';

let logger = new DomLogger();

/**
 * Makes an AudioPool for a PoolDef
 */
export async function makeAudioPool(def: PoolDef): Promise<AudioPool> {
  let binDefs = await getPoolAudioBins(def.audio_bins || []);
  let params = makePoolParams(def);

  let assets = (await Promise.all(binDefs.map(makeAssetsForAudioBin)))
    .reduce((a, b) => a.concat(b), [])

  // format pool
  let pool = new AudioPool({
    assets: assets,
    duration: params.duration,
    dropCount: params.dropCount,
    loop: params.loop
  })

  // this is where bin attributes are applied. BinDef pinned on to Asset when it's
  // created, seek and rate are also applied there.
  pool.play$.subscribe(h => {
    let asset = pool.nowPlaying.find(a => a.id == h.id).asset as BinAsset;

    // apply BinDef configuration
    // volume and fade in:
    doFadeIn(h, asset);

    // panning / positioning:
    doPan(h, asset);

    // optionally stop & fade out when pool ends ("contain" property)
    pool.stop$.pipe(take(1))
      .subscribe(() => {
        if (def.contain) {
          let realVol = doFadeOut(h, asset)
          logger.log(`fading out ${asset.name} (from  ${realVol} to 0) because its parent pool stopped`)
        }
      })

    // global fade in and out on pause / resume
    // (have to figure out a nicer way to do this -- maybe by howl groups instead of individual howls?)
    pool.pause$.subscribe(() => {
      let currentVol = (h.howl as any)._sounds.find(s => s._id == h.id)._volume;
      logger.log(`audio pool paused (${asset.name} currentVol = ${currentVol})`)
      // logger.log(`audio pool paused`)
      h.howl.fade(1, 0, 500);
    })

    pool.resume$.subscribe(() => {
     logger.log("audio pool resumed")
      h.howl.fade(0, 1, 500);
    })

  })

  // logging...
  pool.start$.subscribe(() => logger.log(`audio pool start: ${pool.dropCount} drops, ${dispMs(pool.duration)} long, loop: ${pool.loop}`))
  pool.drop$.subscribe(a => logger.log(`drop ${a+1} / ${pool.dropCount}`))

  return pool
}

/**
 * Performs the initial fade-in for the howl / asset. Call in pool.play$
 * @TODO: this seems broken
 */
function doFadeIn(h: HowlReference, asset: BinAsset) {
    let vol = parseFloat(asset.binDef.volume) || 1
    let fadeInTime = parseFloat(asset.binDef.fade_in_time) || 0
    fadeInTime = Math.round(fadeInTime * 1000)
    if (fadeInTime > 0) {
      h.howl.fade(0, vol, fadeInTime, h.id)
      logger.log(`fading ${asset.name} to ${vol} over ${fadeInTime} ms`)
    } else {
      h.howl.volume(vol, h.id)
    }
}

/**
 * Performs the final fade-out for the howl / asset and stops it when complete.
 * Call in pool.stop$ and whenever sound stops playing completely (eg, timer)
 */
function doFadeOut(h: HowlReference, asset: BinAsset) {
  let fadeOutTime = parseFloat(asset.binDef.fade_out_time) || 0

  // let currentVol = parseFloat(asset.binDef.volume) || 1                    // from config
  let currentVol = (h.howl as any)._sounds.find(s => s._id == h.id)._volume;  // from state
  h.howl.fade(currentVol, 0, fadeOutTime * 1000, h.id)

  // ensure howl is stopped after fading out
  h.howl.once("fade", () => {
    logger.log(`fade finished for ${asset.name}`);
    h.howl.stop(h.id);
  }, h.id)

  return currentVol
}

/**
 * Performs the panning for the howl / asset. Call in pool.play$
 */
function doPan(h: HowlReference, asset: BinAsset) {
  let { start_x, start_y, start_z, start_radius } = asset.binDef;
  let start = makeRandom3dPos({
      x: parseFloat(start_x),
      y: parseFloat(start_y),
      z: parseFloat(start_z)
    },
    parseFloat(start_radius)
  );
  h.howl.pos(start.x, start.y, start.z, h.id);
  logger.log(`playing ${asset.name} at position:`, start);

  if (asset.binDef.has_movement) {
    let { end_x, end_y, end_z, end_radius } = asset.binDef;
    let end = makeRandom3dPos({
        x: parseFloat(end_x),
        y: parseFloat(end_y),
        z: parseFloat(end_z)
      },
      parseFloat(end_radius)
    );
    let { movement_duration_min, movement_duration_max } = asset.binDef;
    let durRange = minMaxToRange(parseFloat(movement_duration_min), parseFloat(movement_duration_max));
    let duration = Math.random() * durRange.range + durRange.offset;

    logger.log(`moving panning ${asset.name} over ${duration.toFixed(1)} seconds to`, end)

    Panner.moveTo({
      howlRef: h,
      startX: start.x,
      startY: start.y,
      startZ: start.z,
      endX: end.x,
      endY: end.y,
      endZ: end.z,
      duration: duration * 1000, // sec to ms
      // @TODO - curve
    })
  }
}

interface Position {
  x: number;
  y: number;
  z: number;
}

/**
 * Generates a random position within a radius of a 3D point
 * Thanks to
 * https://karthikkaranth.me/blog/generating-random-points-in-a-sphere/
 * (we use the "naive" method here because we don't need to be too optimized)
 */
export function makeRandom3dPos(pos: Position, radius: number): Position {
  let { x, y, z } = getRandomInsideUnitSphere();
  let mag = Math.random() * radius;
  x = x * mag + pos.x;
  y = y * mag + pos.y;
  z = z * mag + pos.z;

  return { x, y, z }
}

function getRandomInsideUnitSphere(): Position {
  let d, x, y, z;
  do {
      x = Math.random() * 2.0 - 1.0;
      y = Math.random() * 2.0 - 1.0;
      z = Math.random() * 2.0 - 1.0;
      d = x*x + y*y + z*z;
  } while (d > 1.0);
  return { x, y, z }
};

