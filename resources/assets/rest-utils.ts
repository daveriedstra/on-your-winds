import { Asset } from 'pool-time';

const endpoints = {
  page: 'wp/v2/pages/',
  media: 'wp/v2/media/',
  visualBins: 'wp/v2/visual-bins/',
  audioBins: 'wp/v2/audio-bins/',
  pools: 'wp/v2/pools/',
  binFields: 'acf/v3/bins/',
  poolFields: 'acf/v3/bins/',
};

export type WpPage<T> = {
  acf: T | undefined;
  date: string;
  date_gmt: string;
  guid: {
    rendered: string;
  };
  id: number;
  link: string;
  meta: [];
  modified: string;
  modified_gmt: string;
  slug: string;
  status: string;
  template: string;
  title: {
    rendered: string;
  };
  type: string;
}

export type WpPostObj = {
  ID: number;
  comment_count: string;
  comment_status: string;
  filter: string;
  guid: string;
  menu_order: number;
  ping_status: string;
  pinged: string;
  post_author: string;
  post_content: string;
  post_content_filtered: string;
  post_date: string;
  post_date_gmt: string;
  post_excerpt: string;
  post_mime_type: string;
  post_modified: string;
  post_modified_gmt: string;
  post_name: string;
  post_parent: number;
  post_password: string;
  post_status: string;
  post_title: string;
  post_type: string;
  to_ping: string;
}

export type WpMedia = {
  acf: {
    bins: number[];
  };
  alt_text: string;
  author: number;
  caption: {
    rendered: string;
  };
  comment_status: string;
  date: string;
  date_gmt: string;
  description: {
    rendered: string;
  };
  guid: {
    rendered: string;
  };
  id: number;
  link: string;
  media_details: {
    artist: string;
    album: string;
    width?: string;
    height?: string;
    file?: string;
    sizes?: {
      thumbnail: {
        file: string;
        width: string;
        height: string;
        mime_type: string;
        source_url: string;
      };
      medium: {
        file: string;
        width: string;
        height: string;
        mime_type: string;
        source_url: string;
      };
      medium_large: {
        file: string;
        width: string;
        height: string;
        mime_type: string;
        source_url: string;
      };
      large: {
        file: string;
        width: string;
        height: string;
        mime_type: string;
        source_url: string;
      };
      full: {
        file: string;
        width: string;
        height: string;
        mime_type: string;
        source_url: string;
      };
    };
  };
  media_type: string;
  meta: [];
  mime_type: string;
  modified: string;
  modified_gmt: string;
  ping_status: string;
  post: null
  slug: string;
  source_url: string;
  status: string;
  template: string;
  title: { rendered: string; };
  type: string;
}

export type PoolDef = {
  audio_bins: WpPostObj[];
  visual_bins: WpPostObj[];
  contain: boolean;
  drop_count_min: string;
  drop_count_max: string;
  duration_min: string; // pool duration
  duration_max: string;
  loop: boolean;
  pools: WpPostObj[] | undefined
  chain_drops: boolean;
  drop_duration_min: string;
  drop_duration_max: string;
}

export type AudioBinDef = {
  audio: WpPostObj[];
  sub_bins: WpPostObj[];
  end_radius?: string;
  end_x?: string;
  end_y?: string;
  end_z?: string;
  fade_in_time: string;
  fade_out_time: string;
  has_movement: boolean;
  movement_duration_max?: string;
  movement_duration_min?: string;
  start_radius: string;
  start_x: string;
  start_y: string;
  start_z: string;
  volume: string;
}

export type VisualBinDef = {
  images: WpPostObj[];
  sub_bins: WpPostObj[];
  text: string;

  end_radius?: string;
  end_x?: string;
  end_y?: string;
  has_movement: boolean;
  start_radius: string;
  start_x: string;
  start_y: string;
  z_index: string;

  variable_size: boolean;
  height?: string;
  width?: string;
  height_min?: string;
  width_min?: string;
  height_max?: string;
  width_max?: string;

  fade_in_time: string;
  fade_out_time: string;
  opacity: string;
}

export interface BinAsset extends Asset {
  name: string;
  binDef: AudioBinDef;
}

/**
 * Gets the REST API root URL
 */
let _base;
function getApiBase(): string {
  if (_base === undefined) {
    const linkEl = document.querySelectorAll('link[rel^="https://api.w.org"]');
    if (linkEl.length < 0)
      _base = "/wp-json/";
    else
      _base = (linkEl[0] as any).href;
  }
  return _base;
}

/**
 * Perform an AJAX request with the Fetch API and return the JSON.
 * Room for better error handling here.
 */
async function fetchJson<T>(url: string): Promise<T> {
  let res = await fetch(getApiBase() + url)
  let out;
  if (res.ok)
    out = await res.json()
  return out
}

export async function getPage<T>(page_id: number): Promise<WpPage<T>> {
  return await fetchJson<WpPage<T>>(endpoints.page + page_id)
}

export async function getPool(pool_id: number): Promise<WpPage<PoolDef>> {
  return await fetchJson(endpoints.pools + pool_id)
}

export async function getAudioBin(bin_id: number): Promise<WpPage<AudioBinDef>> {
  return await fetchJson(endpoints.audioBins + bin_id)
}

export async function getVisualBin(bin_id: number): Promise<WpPage<VisualBinDef>> {
  return await fetchJson(endpoints.visualBins + bin_id)
}

export async function getMedia(media_id: number): Promise<WpMedia> {
  return await fetchJson(endpoints.media + media_id)
}
