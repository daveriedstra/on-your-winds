import { PoolDef, AudioBinDef, WpPostObj, WpPage, WpMedia } from './rest-utils';

// each DomLogger instance logs to this container
let domLoggerWindow;

/**
 * Logs to DOM and console only in development builds; removes the DOM elements
 * in production builds.
 */

export class DomLogger {
  private window: Element;
  private isVisible = false;
  private isProd = process.env.NODE_ENV === 'production';

  constructor() {
    if (!this.isProd) {
      if (!domLoggerWindow) {
        domLoggerWindow = this.makeWindow()
      }
      this.window = domLoggerWindow;
      document.body.appendChild(this.window)
    }

    const toggleBtn = document.getElementsByClassName("dom-logger-toggle")[0]
    if (this.isProd) {
      if (!!toggleBtn)
        toggleBtn.remove()
    } else {
      toggleBtn.addEventListener("click", e => this.toggle())
    }
  }

  log(...args) {
    if (!this.isProd) {
      console.dir(...args)
      this.logToDom(args);
    }
  }

  /**
   * Use to sensibly log a pool def page
   */
  logPoolDefPage(p: WpPage<PoolDef>) {
    const logObj = Object.keys(p.acf)
      .filter(k => k != "bins")
      .map(k => `${k}: ${p.acf[k]}`);

    if (!this.isProd) {
      console.dir(p)
      this.logToDom([p.title.rendered, logObj]);
    }
  }

  /**
   * Use to sensibly log a bin def page
   */
  logBinPage(b: WpPage<AudioBinDef>) {
    const logObj = Object.keys(b.acf)
      .map(k => {
        if (k == "audio") {
          return {
            audio: b.acf[k].map(a => a.post_title)
          }
        } else if (k == "sub-bins") {
          return {
            "sub-bins": b.acf[k].map(sb => sb.post_title)
          }
        } else {
          return `${k}: ${b.acf[k]}`
        }
      });

    if (!this.isProd) {
      console.dir(b)
      this.logToDom([b.title.rendered, logObj]);
    }
  }

  private logToDom(args) {
    let d = document.createElement("div");
    d.classList.add("group");

    for (let a of args) {
      const t = (typeof a).toLowerCase()
      let el;
      if (t == "string" || t == "number") {
        el = document.createElement("p")
        el.appendChild(document.createTextNode(a.toString()))
      } else {
        el = document.createElement("pre")
        el.appendChild(document.createTextNode(JSON.stringify(a)))
      }
      d.appendChild(el)
    }
    this.window.appendChild(d)

    // prevent log from bogging down the DOM
    while (this.window.children.length >= 200) {
      this.window.firstChild.remove();
    }

    if (this.isVisible)
      d.scrollIntoView()
  }

  private toggle() {
    const visibleClass = "dom-logger--visible"
    this.isVisible = !this.isVisible

    if (this.isVisible)
      this.window.classList.add(visibleClass)
    else
      this.window.classList.remove(visibleClass)
  }

  private makeWindow() {
    let dlWindow = document.createElement("div")
    dlWindow.classList.add("dom-logger")
    return dlWindow
  }
}
