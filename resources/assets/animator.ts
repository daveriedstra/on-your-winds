/**
 * A one-way animator. These could almost be transitions except that we need to
 * be able to pause them.
 */
export class Animator {
  moveElementTo(args: MoveElementToArgs) {
    let {el, x, y, duration} = args;
    let keyframes = this.makeKeyframes(args);
    el.style['animation-name'] = keyframes.keyframesName;
    el.style['animation-duration'] = duration;
    el.style['animation-iteration-count'] = '1';
    el.style['animation-timing-function'] = 'linear';
    el.dataset['keyframesStyle'] = keyframes.keyframesName;
  }

  /**
   * Makes a stylesheet with a set of keyframes for the specified arguments,
   * adds them to the DOM, and returns the name of the keyframes and a reference
   * to the stylesheet.
   */
  private makeKeyframes(args: MoveElementToArgs): KeyframesStyes {
    let styleEl = document.createElement("style");
    document.head.appendChild(styleEl);

    let kfName = `kf-${performance.now()}`.replace(/\./g, '');
    let keyframes = `
      @keyframes ${kfName} {
        from {
          transform: translate(0, 0);
        }
        to {
          transform: translate(${args.x}, ${args.y});
        }
      }
    `;

    styleEl.appendChild(document.createTextNode(keyframes));
    styleEl.id = kfName;

    return {
      keyframesName: kfName,
      styleElement: styleEl,
    }
  }
}

export interface MoveElementToArgs {
  el: HTMLElement;
  x: string;
  y: string;
  duration: string;
};

interface KeyframesStyes {
  keyframesName: string;
  styleElement: HTMLStyleElement;
};
